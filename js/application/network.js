/*global define*/

/**
 * HTTP module
 * @requires {@link core/http}
 * @requires {@link application/config}
 * @namespace core/http
 * @memberof core
 */

define({
    name: 'application/network',
    requires: [
            'core/http',
            'application/config'
    ],
    def: function network(req) {
        'use strict';

        var http = req.core.http,
            config = req.application.config;

        /**
         * Get next arrives in stop
         * @param idStop stop id
         * @param onSuccess success callback
         * @param onError error callback
         * @param onTimeout timeout callback
         * @param onProgress progress callback
         */

        function getArriveStops(idStop, onSuccess, onError, onTimeout, onProgress) {
            var url = config.getApi('urlBase') + "/geo/GetArriveStop.php",
                data = 'idClient=' + config.getApi('idClient') + '&passKey=' + config.getApi('passKey') + '&idStop=' + idStop + '&cultureInfo=ES';

            http.send({
                url: url,
                async: true,
                method: http.method.POST,
                data: data,
                success: onSuccess,
                error: onError,
                timeout: onTimeout,
                progress: onProgress
            });
        }

        /**
         * Get stops near geolocation
         * @param latitude
         * @param longitude
         * @param radius
         * @param onSuccess success callback
         * @param onError error callback
         * @param onTimeout timeout callback
         * @param onProgress progress callback
         */

        function getStopsFromGeolocation(latitude, longitude, radius, onSuccess, onError, onTimeout, onProgress) {
            var url = config.getApi('urlBase') + "/geo/GetStopsFromXY.php",
                data = 'idClient=' + config.getApi('idClient') + '&passKey=' + config.getApi('passKey') + '&latitude=' + latitude + '&longitude=' + longitude + '&Radius=' + radius + '&cultureInfo=ES';

            http.send({
                url: url,
                async: true,
                method: http.method.POST,
                data: data,
                success: onSuccess,
                error: onError,
                timeout: onTimeout,
                progress: onProgress
            });
        }

        /**
         * Get all stops in bus
         * @param idBus
         * @param onSuccess success callback
         * @param onError error callback
         * @param onTimeout timeout callback
         * @param onProgress progress callback
         */

        function getBusAllStops(idBus, onSuccess, onError, onTimeout, onProgress) {
            var url = config.getApiEmting('urlBase') + "?srv=DatosParada&bus=" + idBus,
                data = 'idClient=' + config.getApiEmting('idClient') + '&passKey=' + config.getApiEmting('passKey'),
                headers = [];

            headers.push({
                name: 'Authorization',
                value: config.getApiEmting('authorization')
            });

            http.send({
                url: url,
                async: true,
                method: http.method.POST,
                data: data,
                headers: headers,
                success: onSuccess,
                error: onError,
                timeout: onTimeout,
                progress: onProgress
            });
        }

        /**
         * Get remaining stops to arrive in bus
         * @param idBus
         * @param onSuccess success callback
         * @param onError error callback
         * @param onTimeout timeout callback
         * @param onProgress progress callback
         */

        function getBusRemainingStops(idBus, onSuccess, onError, onTimeout, onProgress) {
            var url = config.getApiEmting('urlBase') + "?srv=DatosCoche&bus=" + idBus + "&paradas=100",
                data = 'idClient=' + config.getApiEmting('idClient') + '&passKey=' + config.getApiEmting('passKey'),
                headers = [];

            headers.push({
                name: 'Authorization',
                value: config.getApiEmting('authorization')
            });

            http.send({
                url: url,
                async: true,
                method: http.method.POST,
                data: data,
                headers: headers,
                success: onSuccess,
                error: onError,
                timeout: onTimeout,
                progress: onProgress
            });
        }


        return {
            getArriveStops: getArriveStops,
            getStopsFromGeolocation: getStopsFromGeolocation,
            getBusAllStops: getBusAllStops,
            getBusRemainingStops: getBusRemainingStops
        };
    }
});