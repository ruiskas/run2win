/*global define*/

/**
 * Text module
 * @namespace application/text
 * @memberof application
 */

define({
    name: 'application/text',
    def: function helpersText() {
        'use strict';

        var ERROR_CODE = {
	            ERROR: "Error",
	            GENERIC_ERROR: "Se ha producido un error.",
	            DATA_ERROR: "Se ha producido un error con los datos, vuelve a intentarlo.",
	            SERVER_ERROR: "Se ha producido un error con el servidor.",
	            SERVER_TIMEOUT: "Tiempo de espera excedido, vuelve a intentarlo.",
	            GEOLOCATING_TIMEOUT: "Tiempo de espera excedido para acceder a su posición, vuelve a intentarlo más tarde o use la búsqueda manual",
	            GEOLOCATING_ERROR: "Se ha producido un error al acceder a su posición, vuelve a intentarlo más tarde o use la búsqueda manual",
	            UNABLE_GEOLOCATING: "No se ha podido obtener la geolocalización, vuelve a intentarlo más tarde o use la búsqueda manual",
	            GEOLOCATING_DENIED: "No has dado permisos para acceder a tu geolocalización. Habilitalo y vuelve a intentarlo más tarde"
	        },
            TEXT = {
        		BLUETOOTH_POPUP_TITLE: "Paradas",
                BLUETOOTH_POPUP_TEXT: "Debes desconectar el bluetooth, ve a \"Ajustes -> Conexiones -> Bluetooth\" y accede a la wifi del autobus.",
                WIFI_POPUP_TITLE: "Paradas",
                WIFI_POPUP_TEXT: "Para acceder a este servicio, ve a \"Ajustes -> Conexiones -> Wi-Fi\" y conéctate a la wifi del autobus.",
                WIFI_POPUP_ACCEPT: "Aceptar",

                PREV_ARRIVED_POPUP_TITLE: "Próxima parada",
                PREV_ARRIVED_POPUP_TEXT: "La próxima parada es su destino",
                PREV_ARRIVED_NEAR_POPUP_TEXT: "Su destino está próximo",
                ARRIVED_POPUP_TITLE: "Destino",
                ARRIVED_POPUP_TEXT: "Ha llegado a su destino",
                ARRIVED_NEAR_POPUP_TEXT: "Su destino está próximo",
                ARRIVED_POPUP_ACCEPT: "Aceptar",

                LOADING: "Cargando",
                LOCATING: "Obteniendo su localización...",
                LINE: "Línea",
                STOP_NUMBER: "Parada Nº ",
                STOPS: "Paradas",
                WAIT: "Espera",
                REFRESH: "Reintentar",
                NEARME_TITLE: "Espera",
                NEAR_STOPS_TITLE: "Paradas cercanas",
                SEARCH_TITLE: "Busca la parada para ver los tiempos de espera",
                SEARCH: "Buscar",
                QUEST: "Busqueda:",
                RESEARCH: "Reintentar",
                LOCATE_ME: "Localízame",
                LINES: "Lineas",
                MINUTES: "Minutos",
                FIRST_BUS: "1ºBus",
                SECOND_BUS: "2ºBus",
                OUT_OF_ORDER_TITLE: "Fuera de línea",
                OUT_OF_ORDER: "El autobus se encuentra fuera de línea, por favor intentalo más tarde."
            };

        return {
            ERROR_CODE: ERROR_CODE,
            TEXT: TEXT
        };
    }
});