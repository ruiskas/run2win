/*global define*/

/**
 * Config module
 * @namespace application/config
 * @memberof application
 */

define({
    name: 'application/config',
    def: function config() {
        'use strict';

        var _properties = {
        		'default_radius': 250 //250mts by default
        	},
	        _api = {
	            'urlBase': 'https://openbus.emtmadrid.es:9443/emt-proxy-server/last/',
	        	'idClient': 'EMT.SERVICIOS.WEARABLE.SAMSUNG',
	        	'passKey': '9205971B-182B-4640-B149-563DDBE143C2'        		
	        },	
	        _api_emting = {
	            'urlBase': 'http://openbus.emtmadrid.es:8070/rests/',
	            'idClient': 'EMT.SERVICIOS.OPENBUS',
	            'passKey': 'A2C983E5-5BA3-41F3-B47C-428F467041DC',
	            'authorization': 'Basic RU1ULlNFUlZJQ0lPUy5PUEVOQlVTOkEyQzk4M0U1LTVCQTMtNDFGMy1CNDdDLTQyOEY0NjcwNDFEQw==',
	            'emtWifi': /^.*$/
	            //'emtWifi': /^EMT-Madrid$/
	        };

        /**
         * Gets value from configuration.
         * If configuration value doesn’t exists return default value.
         * @param {string} value
         * @param {string} defaultValue
         */
        function get(value, defaultValue) {
            if (_properties[value] !== undefined) {
                return _properties[value];
            }
            return defaultValue;
        }
        
        /**
         * Gets value from API configuration.
         * If configuration value doesn’t exists return default value.
         * @param {string} value
         * @param {string} defaultValue
         */
        function getApi(value, defaultValue) {
            if (_api[value] !== undefined) {
                return _api[value];
            }
            return defaultValue;
        }

        /**
         * Gets value from API Emting configuration.
         * If configuration value doesn’t exists return default value.
         * @param {string} value
         * @param {string} defaultValue
         */
        function getApiEmting(value, defaultValue) {
            if (_api_emting[value] !== undefined) {
                return _api_emting[value];
            }
            return defaultValue;
        }
        
        return {
            get: get,
            getApi: getApi,
            getApiEmting: getApiEmting
        };
    }
});
