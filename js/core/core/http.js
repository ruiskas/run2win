/*global define, console*/

/**
 * HTTP module
 * @requires {@link core/window}
 * @requires {@link core/config}
 * @namespace core/http
 * @memberof core
 */

define({
    name: 'core/http',
    requires: [
        'core/window',
        'core/config'
    ],
    def: function coreHttp(window) {
        'use strict';

        var config = window.core.config,
            _method = {
            'GET' : 'GET',
            'POST' : 'POST',
            'PUT' : 'PUT',
            'DELETE' : 'DELETE'
        };
        
        /**
         * Creates and send request
         * @memberof core/http
         * @param {object} options Options.
         * @param {boolean} [options.async=false] Async mode.
         * @param {string} [options.url] Url.
         * @param {object} [options.data] body data.
         * @param {array} [options.headers] request headers.
         * @param {object} [options.method] request method.
         * @param {object} [options.method_override] request method override (for PUT, DELETE actions).
         * @param {function} [options.success] Success callback.
         * @param {function} [options.error] Error callback.
         * @param {function} [options.timeout] Timeout callback.
         * @param {function} [options.progress] Progress callback.
         * @return {XMLHttpRequest} xhr Request object.
         */
        function send(options) {
            var xhr = null,
                async = null,
                url = null,
                data = null,
                headers = null,
                method = null,
                method_override = null;

            options = options || {};
            async = typeof options.async === 'boolean' ? options.async : false;
            
            url = options.url !== undefined ? options.url : null;
            if (url === null) {
                console.error("http::send error - Url is empty, please provide correct url");
                return;
            }

            data = options.data !== undefined ? options.data : null;       
            headers = options.headers !== undefined ? options.headers : null;       

            method = options.method !== undefined ? options.method : _method.GET;            
            if(method === _method.PUT) {
                method_override = _method.PUT;
                method = _method.POST;
            }
            if(method === _method.DELETE) {
                method_override = _method.DELETE;
                method = _method.POST;
            }

            xhr = new XMLHttpRequest();
            if(async) xhr.timeout = 30000; //30seg timeout
            xhr.open(method, url, async);

            if (method === _method.POST || method === _method.PUT || method === _method.DELETE) {
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                if(method_override) xhr.setRequestHeader("X-HTTP-Method-Override", method_override);
            }
            
            if(headers){
                for(var i=0;i<headers.length;i++) {
                    var header = headers[i];
                    xhr.setRequestHeader(header.name, header.value);
                }
            }


            if (typeof options.success === 'function') {
                xhr.addEventListener('load', function load() {
                    options.success(xhr.response);
                }, false);
            }
            if (typeof options.error === 'function') {
                xhr.addEventListener('error', function error(evt) {
                    console.error("http::send error");
                    options.error(evt.target.status);
                }, false);
            }
            if (typeof options.timeout === 'function') {
                xhr.addEventListener('timeout', function timeout(evt) {
                	console.error("http::send timeout");
                    options.timeout(evt.target.status);
                }, false);
            }
            if (typeof options.progress === 'function') {
                xhr.addEventListener('progress', function progress(evt) {
                    options.progress(evt);
                }, false);
            }


            try {
                xhr.send(data);    
            } 
            catch (e) {
                if (typeof options.error === 'function') {
                    console.log("http::send error");
                    options.error(evt.target.status);
                }
            }
            
            return xhr;
        }

        /**
         * Creates and send request for core
         * @memberof core/http
         * @return {XMLHttpRequest} req Request object.
         */
        function request(options) {
        	options.method = _method.GET;        	
        	return send(options);
        }        

        return {
        	request: request,
        	send: send,
        	method: _method
        };
    }
});
