/*global define*/

/**
 * Config module
 * @namespace core/config
 * @memberof core
 */

define({
    name: 'core/config',
    def: function config() {
        'use strict';

        var _properties = {
            'templateDir': 'templates',
            'templateExtension': '.tpl'
        };

        /**
         * Gets value from configuration.
         * If configuration value doesn’t exists return default value.
         * @memberof core/config
         * @param {string} value
         * @param {string} defaultValue
         */
        function get(value, defaultValue) {
            if (_properties[value] !== undefined) {
                return _properties[value];
            }
            return defaultValue;
        }

        return {
            get: get
        };
    }
});
