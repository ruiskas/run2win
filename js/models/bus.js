/*global define*/
/*jslint plusplus: true*/

/**
 * Bus module
 * @requires {@link core/event}
 * @requires {@link helpers/array}
 * @requires {@link models/section}
 * @namespace models/Bus
 * @memberof models
 */

define({
    name: 'models/bus',
    requires: [
            'core/event',
            'helpers/array',
            'models/section'
    ],
    def: function modelsBus(e) {
        'use strict';

        var arrayHelpers = e.helpers.array,
            Section = e.models.section.Section;

        function Bus(data) {		
            this.lineId = data.linea;
            this.direction = data.sentido;

            this.sections = [];
            if (data.secciones && data.secciones.seccion) {
                var b_sections = arrayHelpers.force2Array(data.secciones.seccion);
                for (var i = 0; i < b_sections.length; i++) {
                    var section = new Section(b_sections[i]);
                    this.sections.push(section);
                }
            }
        }

        return {
            Bus: Bus
        };
    }
});