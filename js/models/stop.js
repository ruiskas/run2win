/*global define*/

/**
 * Stop module
 * @requires {@link core/event}
 * @namespace models/stop
 * @memberof models
 */
define({
    name: 'models/stop',
    requires: ['core/event'],
    def: function modelsStop(e) {
        'use strict';

        function _disable() {
            this.status = "disabled";
        }

        function _enable() {
            this.status = "enabled";
        }

        function _isEnabled() {
            return (this.status === "enabled");
        }

        function Stop(data) {
            this.code = data._codigo;
            this.name = data.__text;
            this.distance = data._distancia;
            this.status = "enabled";


            this.disable = _disable;
            this.enable = _enable;
            this.isEnabled = _isEnabled;
        }

        return {
            Stop: Stop
        };
    }
});