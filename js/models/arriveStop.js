/*global define*/
/*jslint unparam:true*/

/**
 * ArriveStop module
 * @requires {@link core/event}
 * @namespace models/arriveStop
 * @memberof models
 */
define({
    name: 'models/arriveStop',
    requires: [
            'core/event'
    ],
    def: function modelsArriveStop(e) {
        'use strict';

        /**
         * Convert time to minutes format
         */

        function _timeToMinutes(t) {
            var m = 0;

            if (t >= 9999) {
                m = "+20";
            } else if (t === 0) {
                m = ">>";
            } else {
                m = Math.round(t / 60);
            }

            return m;
        }

        function ArriveStop(data) {
            this.stopId = data.stopId;
            this.lineId = data.lineId;
            this.destination = data.destination;
            this.busTimeleft = _timeToMinutes(data.busTimeLeft);
        }

        return {
            ArriveStop: ArriveStop
        };
    }
});