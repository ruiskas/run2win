/*global define*/
/*jslint unparam:true*/

/**
 * Arrive module
 * @requires {@link core/event}
 * @namespace models/arrive
 * @memberof models
 */

define({
    name: 'models/arrive',
    requires: [
       'core/event'
    ],
    def: function modelsArrive(e) {
        'use strict';
        
        function Arrive(data) {
            this.stopId = data.stopId;
            this.lineId = data.lineId;
            this.destination = data.destination;
            this.arrives = [];
        }

        return {
            Arrive: Arrive
        };
    }
});