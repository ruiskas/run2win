/*global define*/
/*jslint plusplus:true*/

/**
 * Section module
 * @requires {@link core/event}
 * @requires {@link helpers/array}
 * @requires {@link models/stop}
 * @namespace models/section
 * @memberof models
 */

define({
    name: 'models/section',
    requires: [
            'core/event',
            'helpers/array',
            'models/stop'
    ],
    def: function modelsSection(e) {
        'use strict';

        var arrayHelpers = e.helpers.array,
            Stop = e.models.stop.Stop;

        function Section(data) {
            this.code = data._codigo;
            this.direction = data._sentido;
            this.length = data._longitud;
            this.name = data._nombre;

            this.stops = [];
            if (data.parada) {
                var s_stops = arrayHelpers.force2Array(data.parada);
                for (var i = 0; i < s_stops.length; i++) {
                    var stop = new Stop(s_stops[i]);
                    this.stops.push(stop);
                }
            }
        }

        function enableStops(e_stops) {
            disableAllStops();
            if (arrayHelpers.isArray(e_stops) && arrayHelpers.isArray(this.stops)) {
                for (var i = 0; i < e_stops.length; i++) {
                    for (var j = 0; i < this.stops.length; j++) {
                        if (e_stops[i].code === this.stops[j].code) {
                            this.stops[j].enable();
                        }
                    }
                }
            }
        }

        function disableAllStops() {
            if (arrayHelpers.isArray(this.stops)) {
                for (var i = 0; i < this.stops.length; i++) {
                    this.stops[i].disable();
                }
            }
        }

        return {
            Section: Section
        };
    }
});