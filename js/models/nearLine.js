/*global define*/
/*jslint unparam:true*/

/**
 * NearLine module
 * @requires {@link core/event}
 * @namespace models/nearLine
 * @memberof models
 */

define({
    name: 'models/nearLine',
    requires: ['core/event'],
    def: function modelsNearLine(e) {
        'use strict';

        function NearLine(data) {
            this.lineId = data.line;
            this.direction = data.direction;

            if (this.direction === "A") {
                this.origin = data.headerA;
                this.destiny = data.headerB;
            } else {
                this.origin = data.headerB;
                this.destiny = data.headerA;
            }
        }

        return {
            NearLine: NearLine
        };
    }
});