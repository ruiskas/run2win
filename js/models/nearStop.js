/*global define*/
/*jslint plusplus:true*/

/**
 * NearStop module
 * @requires {@link core/event}
 * @requires {@link models/nearLine}
 * @namespace models/nearStop
 * @memberof models
 */
define({
    name: 'models/nearStop',
    requires: [
            'core/event',
            'models/nearLine'
    ],
    def: function modelsNearStop(e) {
        'use strict';

        var NearLine = e.models.nearLine.NearLine;

        function NearStop(data) {
            this.stopId = data.stopId;
            this.name = data.name;

            this.lines = [];
            if (data.line) {
                for (var i = 0; i < data.line.length; i++) {
                    var nearLine = new NearLine(data.line[i]);
                    this.lines.push(nearLine);
                }
            }
        }

        return {
            NearStop: NearStop
        };
    }
});