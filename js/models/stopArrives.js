/*global define*/
/*jslint plusplus:true*/

/**
 * Arrives by stop module
 * @requires {@link core/event}
 * @requires {@link models/arrive}
 * @namespace models/stopArrives
 * @memberof models
 */
define({
    name: 'models/stopArrives',
    requires: [
            'core/event',
            'models/arrive'
    ],
    def: function modelsStopArrives(e) {
        'use strict';

        var Arrive = e.models.arrive.Arrive;

        function _addArrive(arriveStop) {
            var index = -1;

            if (this.arrives === null) {
                this.arrives = [];
            }

            //search Arrive by arriveStop.lineId
            for (var i = 0; (index === -1) && i < this.arrives.length; i++) {
                if (arriveStop.lineId === this.arrives[i].lineId) {
                    index = i;
                }
            }

            //add new Arrive with arriveStop inside
            if (index === -1) {
                var arrive = new Arrive({
                    stopId: arriveStop.stopId,
                    lineId: arriveStop.lineId,
                    destination: arriveStop.destination
                });
                arrive.arrives.push(arriveStop);

                this.arrives.push(arrive);
            }
            //update Arrive
            else {
                this.arrives[index].arrives.push(arriveStop);

                //sort arriveStop into arrives
                this.arrives[index].arrives.sort(function(a, b) {
                    return a.busTimeleft - b.busTimeleft;
                });
            }
        }

        function StopArrives(idStop) {
            this.stopId = idStop;
            this.arrives = [];

            this.addArrive = _addArrive;
        }

        return {
            StopArrives: StopArrives
        };
    }
});