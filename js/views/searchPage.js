/*global define, tau, document, window*/
/*jslint plusplus:true*/

/**
 * Search page module
 * @requires {@link core/event}
 * @requires {@link core/template}
 * @requires {@link core/config}
 * @requires {@link helpers/dateTime}
 * @requires {@link models/arriveStop}
 * @requires {@link models/stopArrives}
 * @requires {@link application/text}
 * @requires {@link application/network}
 * @namespace views/searchPage
 * @memberof views
 */
define({
    name: 'views/searchPage',
    requires: [
            'core/event',
            'core/template',
            'core/config',
            'helpers/dateTime',
            'models/arriveStop',
            'models/stopArrives',
            'application/text',
            'application/network'
    ],
    def: function viewsSearchPage(req) {
        'use strict';

        var e = req.core.event,
            tpl = req.core.template,
            http = req.application.network,
            dateTime = req.helpers.dateTime,
            texts = req.application.text,

            StopArrives = req.models.stopArrives.StopArrives,
            ArriveStop = req.models.arriveStop.ArriveStop,

            page = null,
            pageId = 'search-page',

            //time
            title_time = document.getElementById('search-title-time'),
            title_date = document.getElementById('search-title-date'),

            //loading
            loading_section = document.getElementById('search-loading'),
            loading_text = document.getElementById('search-loading-text'),

            //error
            error_subheader_title = document.getElementById('search-error-subheader-title'),
            error_section = document.getElementById('search-error'),
            error_title = document.getElementById('search-error-title'),
            error_text = document.getElementById('search-error-text'),

            //content
            search_btn = document.getElementById('search-navi-arrives'),
            search_text = document.getElementById('search-input-text'),

            search_refresh_btn = document.getElementById('search-refresh-btn'),

            searchResultsContent = document.getElementById('search-content'),

            searchResultsContentList = document.getElementById('arriveDetails-content-arrives-list'),

            idStop = null,

            stopArrives = null,
            searchList = null,

            initialised = false;


        //**************************************************************************************//
        //************************************EVENT HANDLERS************************************//
        //**************************************************************************************//

        /**
         * Handles search by stopId text value change
         * @param {event} ev
         */

        function onSearchTextChange(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            var text = search_text.value;
            if (text) text = text.trim();

            if (text === "") search_btn.disabled = true;
            else search_btn.disabled = false;
        }

        /**
         * Handles search by stopId input text onFocus
         * @param {event} ev
         */

        function onSearchTextFocus(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            search_text.value = "";
        }

        /**
         * Handles navigation stops button tap
         * @param {event} ev
         */

        function onRefreshStopsTap(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            showData();
        }

        /**
         * Handles navigation timer button tap
         * @param {event} ev
         */

        function onNaviSearchTap(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            searchStopById();
        }

        function _bindEvents() {
            search_text.addEventListener(
                'input',
                onSearchTextChange);
            search_text.addEventListener(
                'focus',
                onSearchTextFocus);

            search_btn.addEventListener(
                'click',
                onNaviSearchTap);

            search_refresh_btn.addEventListener(
                'click',
                onRefreshStopsTap);
        }


        //**************************************************************************************//
        //*************************************PAGE METHODS*************************************//
        //**************************************************************************************//

        /**
         * Search stop by id.
         */

        function searchStopById() {
            idStop = search_text.value;
            if ((idStop !== "") && (idStop > 0)) {
                showLoading();

                http.getArriveStops(idStop, function(response) {
                    console.log("searchPage::getArriveStops onSuccess");
                    var result = JSON.parse(response);

                    searchList = searchList || document.getElementById('arriveDetails-list');
                    while (searchList.firstChild) {
                        searchList.removeChild(searchList.firstChild);
                    }

                    if (idStop) document.getElementById('arriveDetails-stop-id').innerHTML = texts.TEXT.STOP_NUMBER + " " + idStop;

                    if (result.arrives) {
                        stopArrives = new StopArrives(idStop);

                        for (var i = 0; i < result.arrives.length; i++) {
                            var arriveStop = new ArriveStop(result.arrives[i]);
                            stopArrives.addArrive(arriveStop);
                        }

                        for (var i = 0; i < stopArrives.arrives.length; i++) {
                            var arrive = stopArrives.arrives[i];

                            if (arrive.arrives.length >= 2) {
                                var html = tpl.get('arriveRow', {
                                    line: arrive.lineId,
                                    destination: arrive.destination,
                                    firstTimeLeft: arrive.arrives[0].busTimeleft,
                                    secondTimeLeft: arrive.arrives[1].busTimeleft
                                });
                                var tmp_element = document.createElement('li');
                                tmp_element.innerHTML = html;
                                searchList.appendChild(tmp_element.firstChild);
                            }
                        }

                        searchResultsContentList.scrollTop = 0;

                        // change page to searchResult
                        e.fire('changeActivePage');
                        tau.changePage('#arriveDetails-page');
                        showData();
                    } else showError(texts.ERROR_CODE.DATA_ERROR);
                }, function(status) {
                    console.error("searchPage::getArriveStops onError");
                    //console.debug(JSON.stringify(status));
                    showError(texts.ERROR_CODE.SERVER_ERROR);
                }, function(status) {
                    console.error("searchPage::getArriveStops onTimeout");
                    //console.debug(JSON.stringify(status));
                    showError(texts.ERROR_CODE.SERVER_TIMEOUT);
                }, function(evt) {
                    //console.log("searchPage::getArriveStops onProgress");
                    //console.debug(JSON.stringify(evt));
                });
            }
        }

        //**************************************************************************************//
        //**********************************SHOW/HIDE SECTIONS**********************************//
        //**************************************************************************************//

        function showData() {
            hideLoading();
            hideError();

            searchResultsContent.hidden = false;
        }

        function hideData() {
            searchResultsContent.hidden = true;
        }

        function showError(msg, title) {
            hideLoading();
            hideData();

            error_section.hidden = false;
            error_title.innerHTML = title || texts.ERROR_CODE.ERROR;
            error_text.innerHTML = msg || texts.ERROR_CODE.DATA_ERROR;

            error_subheader_title.innerHTML = texts.TEXT.QUEST + " \"" + idStop + "\"";
            search_text.value = "";
            search_btn.disabled = true;
        }

        function hideError() {
            error_section.hidden = true;
        }

        function showLoading(msg) {
            hideError();
            hideData();

            loading_section.hidden = false;
            loading_text.innerHTML = msg || texts.TEXT.LOADING;

            search_btn.disabled = true;
        }

        function hideLoading() {
            loading_section.hidden = true;
        }

        //**************************************************************************************//
        //***********************************INIT PAGE METHODS**********************************//
        //**************************************************************************************//

        /**
         * Show the search page.
         */

        function _show() {
            page = page || document.getElementById(pageId);
            tau.changePage(page);
        }

        /**
         * Refresh title datetime
         */

        function refreshDatetime() {
            dateTime.refreshDatetime(function(currentTime) {
                title_time.innerHTML = currentTime;
            });
        }

        /**
         * Initialise the stops - timer and events.
         *
         * @return {boolean} True if any action was performed.
         */

        function _init() {
            search_text.value = "";

            if (initialised) {
                return false;
            }

            // init UI by binding events
            _bindEvents();

            initialised = true;
            return true;
        }

        function _pageShow() {
            title_time.innerHTML = dateTime.getTime();
            title_date.innerHTML = dateTime.getDate();
            search_refresh_btn.innerHTML = texts.TEXT.RESEARCH;
            document.getElementById('search-title').innerHTML = texts.TEXT.SEARCH_TITLE;
            search_btn.innerHTML = texts.TEXT.SEARCH;

            showData();
            refreshDatetime();
            _init();
        }

        /**
         * Bind the pageshow event.
         */

        function _bindPageShow() {
            page = page || document.getElementById(pageId);

            page.addEventListener('pageshow', _pageShow);

            // if (page.classList.contains('ui-page')) {
            //     // the page is already active and the handler didn't run
            //     _pageShow();
            // }
        }

        e.listeners({
            'views.searchPage.show': _show
        });
        
        return {
            init: _bindPageShow
        };
    }
});