/*global define, tau, document, window*/
/*jslint plusplus:true*/

/**
 * Stops page module
 * @requires {@link core/event}
 * @requires {@link core/template}
 * @requires {@link core/config}
 * @requires {@link helpers/dateTime}
 * @requires {@link helpers/timer}
 * @requires {@link helpers/array}
 * @requires {@link models/bus}
 * @requires {@link models/stop}
 * @requires {@link application/text}
 * @requires {@link application/network}
 * @namespace views/stopsPage
 * @memberof views
 */
define({
    name: 'views/stopsPage',
    requires: [
            'core/event',
            'core/template',
            'core/config',
            'helpers/dateTime',
            'helpers/timer',
            'helpers/array',
            'models/bus',
            'models/stop',
            'application/text',
            'application/network'
    ],
    def: function viewsStopsPage(req) {
        'use strict';

        var e = req.core.event,
            tpl = req.core.template,
            http = req.application.network,
            dateTime = req.helpers.dateTime,
            timer = req.helpers.timer,
            texts = req.application.text,
            arrayHelpers = req.helpers.array,

            Bus = req.models.bus.Bus,
            Stop = req.models.stop.Stop,

            page = null,
            pageId = 'stops-page',

            //time
            title_time = document.getElementById('stops-title-time'),
            title_date = document.getElementById('stops-title-date'),

            //loading
            loading_section = document.getElementById('stops-loading'),
            loading_text = document.getElementById('stops-loading-text'),

            //error
            error_section = document.getElementById('stops-error'),
            error_title = document.getElementById('stops-error-title'),
            error_text = document.getElementById('stops-error-text'),

            //content
            stopsContent = document.getElementById('stops-content'),
            stopContentList = document.getElementById('stops-content-bus-list'),

            //pop-up
            popup_arrive = document.getElementById('popup'),
            popup_arrive_title_text = document.getElementById('popup-title'),
            popup_arrive_content_text = document.getElementById('popup-content'),

            stops_refresh_btn = document.getElementById('stops-refresh-btn'),

            stopsList = null,
            bus = null,
            way = null,

            prevSelectedStop = null,
            selectedStop = null,
            checkedStop = null,
            prevNear_warned = false,
            destNear_warned = false,
            
            idBus = null,

            initialised = false;


        //**************************************************************************************//
        //************************************EVENT HANDLERS************************************//
        //**************************************************************************************//

        /**
         * Click on enabled Stop list element
         * @param {event} ev 
         */

        function onStopClicked(ev) {
            //if clicked outside checkbox prevent default and stop propagation event
            if (ev.target && ev.target.name !== "radio-stop") {  
                ev.stopPropagation();
                ev.preventDefault();
            }

            if (ev.currentTarget && ev.currentTarget.id) {
                var stop_splitted = ev.currentTarget.id.split('_');
                if (stop_splitted.length === 2) {
                    if (stop_splitted[1] !== selectedStop) {
                        checkedStop = selectedStop = stop_splitted[1];
                        document.getElementById("stop_" + selectedStop + "_checkbox").checked = true;

                        //save selectedStop in localstorage
                        localStorage.setItem("selectedStop", selectedStop);
                        
                        var stop = null,
                            prevStop = null;
                        //search for previous stop
                        for (var i = 0; !stop && i < way.stops.length; i++) {
                            var prev = way.stops[i - 1];
                            var current = way.stops[i];
                            if (current.code == selectedStop) {
                                stop = current;
                                prevStop = prev;
                                
                                prevNear_warned = false;
                                destNear_warned = false;
                                break;
                            }
                        }

                        if (prevStop !== null) {
                            prevSelectedStop = prev.code;
                            //save prevSelectedStop in localstorage
                            localStorage.setItem("prevSelectedStop", prevSelectedStop);

                            //warn user if prevSelectedStop yet visited
                            if (!prevStop.isEnabled()) destinyPrevArrived(prevStop.name);
                        }
                    }
                } else selectedStop = null;
            }
        }

        /**
         * Handles navigation stops button tap
         * @param {event} ev
         */

        function onRefreshStopsTap(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            getAllStops();
        }

        function _bindEvents() {
            stops_refresh_btn.addEventListener(
                'click',
                onRefreshStopsTap);

            document.getElementById("popup_accept_btn")
                .addEventListener("click", function() {
                tau.closePopup();
            }, false);
        }


        //**************************************************************************************//
        //*************************************PAGE METHODS*************************************//
        //**************************************************************************************//

        /**
         * Get all stops for a bus.
         */

        function getAllStops() {
            if (idBus) {
                showLoading();

                http.getBusAllStops(idBus, function(response) {
                    console.log("stopsPage::getAllStops onSuccess");
                    var result = xml2json.xml_str2json(response);

                    if (result.DatosParada) {
                        bus = new Bus(result.DatosParada);
                        if (bus.lineId) document.getElementById('stops-line-id').innerHTML = texts.TEXT.LINE.toUpperCase() + " " + bus.lineId;

                        //check bus direction to set the travel way
                        if (bus.direction) getRemainingStops();
                        else showError(texts.TEXT.OUT_OF_ORDER, texts.TEXT.OUT_OF_ORDER_TITLE);
                    } else showError(texts.ERROR_CODE.DATA_ERROR);
                }, function(status) {
                    console.error("stopsPage::getAllStops onError");
                    //console.debug(JSON.stringify(status));
                    showError(texts.ERROR_CODE.SERVER_ERROR);
                }, function(status) {
                    console.error("stopsPage::getAllStops onTimeout");
                    //console.debug(JSON.stringify(status));
                    showError(texts.ERROR_CODE.SERVER_TIMEOUT);
                }, function(evt) {
                    //console.log("stopsPage::getAllStops onProgress");
                    //console.debug(JSON.stringify(evt));
                });
            } else showError(texts.ERROR_CODE.DATA_ERROR);
        }

        /**
         * Get remaining stops for a bus.
         */

        function getRemainingStops() {
            if (idBus) {
                http.getBusRemainingStops(idBus, function(response) {
                    console.log("stopsPage::getRemainingStops onSuccess");
                    var result = xml2json.xml_str2json(response);

                    stopsList = stopsList || document.getElementById('stops-list');
                    //clean all stops
                    while (stopsList.firstChild) {
                        stopsList.removeChild(stopsList.firstChild);
                    }

                    if (result.DatosCoche && result.DatosCoche.sentido) {
                        //check direction from remainingStops, not from bus (busAllStops doesn't refresh)
                        var direction = result.DatosCoche.sentido;
                        for (var i = 0; i < bus.sections.length; i++) {
                            if (bus.sections[i].direction === direction) way = bus.sections[i];
                        }

                        if ((way && way.stops) && (result.DatosCoche && result.DatosCoche.paradas && result.DatosCoche.paradas.parada)) {
                        	//initially set all stops of way to 'disabled' (ie 'visited')
                            for (var i = 0; i < way.stops.length; i++) way.stops[i].disable();
                            
                        	var remaining = arrayHelpers.force2Array(result.DatosCoche.paradas.parada);
                            var remainingStops = [];

                            for (var i = 0; i < remaining.length; i++) {
                                var stop = new Stop(remaining[i]);
                                remainingStops.push(stop);
                            }

                            //view remainingStop to set stops 'enabled'
                            for (var i = 0; i < remainingStops.length; i++) {
                                for (var j = 0; j < way.stops.length; j++) {
                                    if (remainingStops[i].code === way.stops[j].code) {
                                    	way.stops[j].distance = remainingStops[i].distance; 
                                        way.stops[j].enable();
                                        break;
                                    }
                                }
                            }

                            var firstEnabled = null,
                                Index = null;
                            //print data
                            for (var i = 0; i < way.stops.length; i++) {
                                var stop = way.stops[i];
                                var html = tpl.get('stopRow', {
                                    code: stop.code,
                                    name: stop.name,
                                    status: stop.status,
                                    checked: (stop.code === checkedStop) ? "checked" : ""
                                });
                                var tmp_element = document.createElement('li');
                                tmp_element.innerHTML = html;
                                stopsList.appendChild(tmp_element.firstChild);

                                if (stop.isEnabled()) {
                                    if (firstEnabled == null) firstEnabled = i;

                                    document.getElementById('stop_' + stop.code)
                                        .addEventListener("click", onStopClicked);
                                }

                                //always scroll to selectedStop
                                //if(stop.code === selectedStop) firstEnabled = i;
                            }

                            //check selected stop to warn user
                            if (result.DatosCoche.enParada) {
                                var currentStop = new Stop(result.DatosCoche.enParada);

                                if (currentStop.code === selectedStop) destinyArrived(currentStop.name);
                                else if (currentStop.code === prevSelectedStop) destinyPrevArrived(currentStop.name);
                                else checkVisitedStops();
                            } 
                            //check all visited stops
                            else checkVisitedStops();
                            
                            refreshBusRemainingData();
                            showData();

                            //autoscroll to enabled area
                            if (firstEnabled > 0) {
                                var stopsListItems = stopsList.children;
                                stopsContent.scrollTop = (stopsListItems[firstEnabled].offsetTop - stopContentList.offsetTop);
                            } 
                            else stopContentList.scrollTop = 0;

                        } 
                        else showError(texts.ERROR_CODE.DATA_ERROR);
                    } 
                    else showError(texts.ERROR_CODE.DATA_ERROR);
                }, function(status) {
                    console.error("stopsPage::getRemainingStops onError");
                    //console.debug(JSON.stringify(status));
                    showError(texts.ERROR_CODE.SERVER_ERROR);
                }, function(status) {
                    console.error("stopsPage::getRemainingStops onTimeout");
                    //console.debug(JSON.stringify(status));
                    showError(texts.ERROR_CODE.SERVER_TIMEOUT);
                }, function(evt) {
                    //console.log("stopsPage::getRemainingStops onProgress");
                    //console.debug(JSON.stringify(evt));
                });
            } else showError(texts.ERROR_CODE.DATA_ERROR);
        }

        /**
         * Refresh remaining stops status
         */

        function refreshBusRemainingData() {
            //refresh status each 10sec
            if (timer.isRunning()) timer.stopTimerCallback();
            timer.launchTimerCallback(function() {
                getRemainingStops();
            }, 10000);
        }

        function checkVisitedStops(){
        	var nearStop = null;
        	var stop = null;
            var prevStop = null;
            for (var i = 0; !stop && i < way.stops.length; i++) {
                var prev = way.stops[i-1];
                var current = way.stops[i];
                var next = way.stops[i+1];
                
                if (current && (current.code === selectedStop)) {
                	if(current.distance <= 200) nearStop = current;
                	else{
                        prevStop = prev;
                		stop = current;
                	}	
                }
                else if(current && (current.code === prevSelectedStop)) {
                    if(current.distance <= 200) nearStop = current;
                    else {
                    	prevStop = current;
                    	stop = next;
                    }
                }
            }

            if ((stop !== null) && (current.code === selectedStop)) {
                if (!stop.isEnabled()) destinyArrived(stop.name);
            } 
            else if ((prevStop !== null) && (current.code === prevSelectedStop)) {
                if (!prevStop.isEnabled()) destinyPrevArrived(prevStop.name);
            } 
            else if (nearStop !== null) {            	
                if (nearStop.code === prevSelectedStop) destinyPrevArrivingNear(nearStop.name);
                else if (nearStop.code === selectedStop) destinyArrivingNear(nearStop.name);
            }
        }
        
        /**
         * Warn user next arrive is destiny with popup
         */

        function destinyPrevArrivingNear(stop_name) {
            //console.debug("stopPage::destinyNextArrivingNear");
        	if(!prevNear_warned) { //not spam user with several warnings
	            popup_arrive_title_text.innerHTML = texts.TEXT.PREV_ARRIVED_POPUP_TITLE.toUpperCase();
	            popup_arrive_content_text.innerHTML = "\"" + stop_name + "\" " + texts.TEXT.PREV_ARRIVED_NEAR_POPUP_TEXT.toLowerCase();
	    		
	            /*1sec ON, .5sec OFF, 1sec ON*/
	            showWarning([1000, 500, 1000]);  
	            
	            prevNear_warned = true;
        	}
        }
        
        /**
         * Warn user next arrive is destiny with popup
         */

        function destinyPrevArrived(stop_name) {
            //console.debug("stopPage::destinyPrevArrived");
            
            popup_arrive_title_text.innerHTML = texts.TEXT.PREV_ARRIVED_POPUP_TITLE.toUpperCase();
            popup_arrive_content_text.innerHTML = "\"" + stop_name + "\" " + texts.TEXT.PREV_ARRIVED_POPUP_TEXT.toLowerCase();

            /*1sec ON, .5sec OFF, 1sec ON*/
            showWarning([1000, 500, 1000]); 
            
            prevSelectedStop = null;
            //clear prevSelectedStop from localstorage
            localStorage.setItem("prevSelectedStop", prevSelectedStop);
        }
        
        /**
         * Warn user has arrived destiny
         */

        function destinyArrivingNear(stop_name) {
            //console.debug("stopPage::destinyArrivingNear");
        	if(!destNear_warned) { //not spam user with several warnings
        		popup_arrive_title_text.innerHTML = texts.TEXT.ARRIVED_POPUP_TITLE.toUpperCase();
                popup_arrive_content_text.innerHTML = texts.TEXT.ARRIVED_NEAR_POPUP_TEXT + " \"" + stop_name + "\"";
        		
                /*1sec ON, .5sec OFF, 1sec ON*/
                showWarning([1000, 500, 1000]);             

                destNear_warned = true;
        	}
        }
        
        /**
         * Warn user has arrived destiny
         */

        function destinyArrived(stop_name) {
            //console.debug("stopPage::destinyArrived");

            popup_arrive_title_text.innerHTML = texts.TEXT.ARRIVED_POPUP_TITLE.toUpperCase();
            popup_arrive_content_text.innerHTML = texts.TEXT.ARRIVED_POPUP_TEXT + " \"" + stop_name + "\"";
            
            /*1sec ON, .5sec OFF, 2sec ON, .5sec OFF, 1sec ON*/
            showWarning([1000, 500, 2000, 500, 1000]);            
            
            //timer.stopTimerCallback();
            selectedStop = null;
            //clear selectedStop from localstorage
            localStorage.setItem("selectedStop", selectedStop);
        }
        
        /**
         * Launch app to foreground to show ui warnings to user
         */
        function showWarning(arr_vibration) {
        	if(tizen && tizen.application && tizen.application.getAppInfo){
        		//launch app to foreground
        		tizen.application.launch(tizen.application.getAppInfo().id);
        	}
        	if(tizen && tizen.power && tizen.power.isScreenOn && tizen.power.turnScreenOn){
        		//turn on screen if lock screen is active
        		if(!tizen.power.isScreenOn()) tizen.power.turnScreenOn();
        	}

        	tau.closePopup();
        	timer.launchTimerCallback(function() {
        		tau.openPopup("#popup");        		
        		if(arr_vibration && arrayHelpers.isArray(arr_vibration)) navigator.vibrate(arr_vibration);
            }, 1000);
        }
        //**************************************************************************************//
        //**********************************SHOW/HIDE SECTIONS**********************************//
        //**************************************************************************************//

        function showData() {
            hideLoading();
            hideError();
            stopsContent.hidden = false;
        }

        function hideData() {
            stopsContent.hidden = true;
        }

        function showError(msg, title) {
            hideLoading();
            hideData();

            error_section.hidden = false;
            error_title.innerHTML = title || texts.ERROR_CODE.ERROR;
            error_text.innerHTML = msg || texts.ERROR_CODE.DATA_ERROR;
        }

        function hideError() {
            error_section.hidden = true;
        }

        function showLoading(msg) {
            hideError();
            hideData();

            loading_section.hidden = false;
            loading_text.innerHTML = msg || texts.TEXT.LOADING;
        }

        function hideLoading() {
            loading_section.hidden = true;
        }


        //**************************************************************************************//
        //***********************************INIT PAGE METHODS**********************************//
        //**************************************************************************************//

        /**
         * Show the stops page.
         */

        function _show() {
            page = page || document.getElementById(pageId);
            tau.changePage(page);
        }

        /**
         * Refresh title datetime
         */

        function refreshDatetime() {
            dateTime.refreshDatetime(function(currentTime) {
                title_time.innerHTML = currentTime;
            });
        }

        /**
         * Initialise the stopsPage.
         * @return {boolean} True if any action was performed.
         */

        function _init() {
            //TO-DO: delete idBus when test it into EMT's wifi
        	idBus = 4283;
            document.getElementById('popup_accept_btn').innerHTML = texts.TEXT.ARRIVED_POPUP_ACCEPT.toUpperCase();

            try {
                getAllStops();
            } catch (e) {
                showError(texts.ERROR_CODE.DATA_ERROR);
            }

            if (initialised) {
                return false;
            }

            //load selected stops from local storage
            prevSelectedStop = localStorage.getItem("prevSelectedStop");
            selectedStop = localStorage.getItem("selectedStop");

            // init UI by binding events
            _bindEvents();

            initialised = true;
            return true;
        }

        function _pageShow() {
            title_time.innerHTML = dateTime.getTime();
            title_date.innerHTML = dateTime.getDate();
            stops_refresh_btn.innerHTML = texts.TEXT.REFRESH;
            refreshDatetime();
            _init();
        }

        /**
         * Bind the pageshow event.
         */

        function _bindPageShow() {
            page = page || document.getElementById(pageId);
            page.addEventListener('pageshow', _pageShow);
            // if (page.classList.contains('ui-page')) {
            //     // the page is already active and the handler didn't run
            //     _pageShow();
            // }
        }

        e.listeners({
            'views.stopsPage.show': _show
        });

        return {
            init: _bindPageShow
        };
    }
});