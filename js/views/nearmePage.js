/*global define, tau, document, window, navigator*/
/*jslint plusplus:true*/

/**
 * Nearme page module
 * @requires {@link core/event}
 * @requires {@link core/template}
 * @requires {@link core/config}
 * @requires {@link helpers/dateTime}
 * @requires {@link models/nearStop}
 * @requires {@link models/arriveStop}
 * @requires {@link models/stopArrives}
 * @requires {@link application/text}
 * @requires {@link application/config}
 * @requires {@link application/network}
 * @namespace views/nearmePage
 * @memberof views
 */
define({
    name: 'views/nearmePage',
    requires: [
            'core/event',
            'core/template',
            'core/config',
            'helpers/dateTime',
            'models/nearStop',
            'models/arriveStop',
            'models/stopArrives',
            'application/text',
            'application/config',
            'application/network'
    ],
    def: function viewsNearmePage(req) {
        'use strict';

        var e = req.core.event,
            tpl = req.core.template,
            http = req.application.network,
            config = req.application.config,
            dateTime = req.helpers.dateTime,
            texts = req.application.text,

            StopArrives = req.models.stopArrives.StopArrives,
            ArriveStop = req.models.arriveStop.ArriveStop,
            NearStop = req.models.nearStop.NearStop,

            page = null,
            pageId = 'nearme-page',

            //time
            title_time = document.getElementById('nearme-title-time'),
            title_date = document.getElementById('nearme-title-date'),

            //loading
            loading_section = document.getElementById('nearme-loading'),
            loading_text = document.getElementById('nearme-loading-text'),

            //error
            error_section = document.getElementById('nearme-error'),
            error_title = document.getElementById('nearme-error-title'),
            error_text = document.getElementById('nearme-error-text'),

            //content
            nearmeContent = document.getElementById('nearme-content'),
            nearmeContentList = document.getElementById('nearme-content-arrives-list'),

            searchResultsContentList = document.getElementById('arriveDetails-content-arrives-list'),

            search_btn = document.getElementById('nearme-navi-search'),
            locateme_btn = document.getElementById('nearme-locateme'),

            nearmeList = null,
            stopArrives = null,
            searchList = null,

            default_radius = config.get('default_radius'),
            geolocating = false,

            initialised = false;

        //**************************************************************************************//
        //************************************EVENT HANDLERS************************************//
        //**************************************************************************************//

        /**
         * Handles search navi button tap
         * @param {event} ev
         */

        function onNaviSearchTap(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            // change page to Search
            e.fire('changeActivePage');
            tau.changePage('#search-page');
        }

        /**
         * Handles geolocation refresh button tap
         * @param {event} ev
         */

        function onNaviLocatemeTap(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            if (!geolocating) {
                refreshGeolocation();
            }
        }

        /**
         * Click on nearme Stop list element
         * @param {event} ev 
         */

        function onNearmeStopClicked(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            if (ev.currentTarget && ev.currentTarget.id) {
                var near_stop_splitted = ev.currentTarget.id.split('_');
                if (near_stop_splitted.length === 2) {
                    var idStop = near_stop_splitted[1];
                    getArrivesFromStop(idStop);
                }
            }
        }


        function _bindEvents() {
            search_btn.addEventListener('click', onNaviSearchTap);
            locateme_btn.addEventListener('click', onNaviLocatemeTap);
        }


        //**************************************************************************************//
        //*************************************PAGE METHODS*************************************//
        //**************************************************************************************//

        /**
         * Request for device geolocation
         */

        function refreshGeolocation() {
            showLocating();

            //TO-DO: for testing geolocation
	          //getStops("40.4202591", "-3.70485199999996"); //cruce C/Gran Via con C/Concepción Arenal
	          //getStops("40.4105989", "-3.6694938000000548"); //C/Doctor Esquerdo 8
            
            if (navigator && navigator.geolocation && navigator.geolocation.getCurrentPosition) {
            	navigator.geolocation.getCurrentPosition(
            			function(position){
            				var latitude = position.coords.latitude;
            				var longitude = position.coords.longitude;
            				
            				if (latitude === 0 && longitude === 0) showError(texts.ERROR_CODE.UNABLE_GEOLOCATING);
            	            else getStops(latitude, longitude);
            			},
            			function(err){
            				switch (err.code) 
            				{
            					case err.PERMISSION_DENIED:
            						locateme_btn.disabled = false;
            						showError(texts.ERROR_CODE.GEOLOCATING_DENIED);
	        				        break;
            					case err.POSITION_UNAVAILABLE:
            						locateme_btn.disabled = false;
            						showError(texts.ERROR_CODE.UNABLE_GEOLOCATING);
            						break;
            					case err.TIMEOUT:
            						showError(texts.ERROR_CODE.GEOLOCATING_TIMEOUT);
            						break;
            					case err.UNKNOWN_ERROR:
            						showError(texts.ERROR_CODE.GEOLOCATING_ERROR);
            						break;
            				}
            			},
            			{
            			  enableHighAccuracy: false, 
      					  timeout: 60000, //1min
      					  maximumAge: 0
      					}
            	);
            }
            else showError(texts.ERROR_CODE.GEOLOCATING_ERROR);
        }

        /**
         * Request server for stops by position
         * @param latitude
         * @param longitude
         */

        function getStops(latitude, longitude) {
            showLoading();

            http.getStopsFromGeolocation(latitude, longitude, default_radius, function(response) {
                console.log("nearme::getStopsFromGeolocation onSuccess");
                var result = JSON.parse(response);

                nearmeList = nearmeList || document.getElementById('nearme-list');
                while (nearmeList.firstChild) {
                    nearmeList.removeChild(nearmeList.firstChild);
                }

                document.getElementById('nearme-title').innerHTML = texts.TEXT.NEAR_STOPS_TITLE.toUpperCase();

                if (result.stop) {
                    for (var i = 0; i < result.stop.length; i++) {
                        var nearStop = new NearStop(result.stop[i]);

                        var html = tpl.get('nearStopRow', {
                            stopId: nearStop.stopId,
                            name: nearStop.name
                        });
                        var tmp_element = document.createElement('li');
                        tmp_element.innerHTML = html;
                        nearmeList.appendChild(tmp_element.firstChild);

                        document.getElementById('nearme_' + nearStop.stopId)
                            .addEventListener("click", onNearmeStopClicked);
                    }
                    nearmeContentList.scrollTop = 0;

                    showData();
                } else showError(texts.ERROR_CODE.DATA_ERROR);
            }, function(status) {
                console.error("nearmePage::getStopsFromGeolocation onError");
                //console.debug(JSON.stringify(status));
                showError(texts.ERROR_CODE.SERVER_ERROR);
            }, function(status) {
                console.error("nearmePage::getStopsFromGeolocation onTimeout");
                //console.debug(JSON.stringify(status));
                showError(texts.ERROR_CODE.SERVER_TIMEOUT);
            }, function(evt) {
                //console.log("searchPage::getArriveStops onProgress");
                //console.debug(JSON.stringify(evt));
            });
        }

        /**
         * Request server for arrives from stopId
         * @param idStop stopId
         */

        function getArrivesFromStop(idStop) {
            showLoading();

            http.getArriveStops(idStop, function(response) {
                console.log("nearmePage::getArrivesFromStop onSuccess");
                var result = JSON.parse(response);

                searchList = searchList || document.getElementById('arriveDetails-list');
                while (searchList.firstChild) {
                    searchList.removeChild(searchList.firstChild);
                }

                if (idStop) document.getElementById('arriveDetails-stop-id').innerHTML = texts.TEXT.STOP_NUMBER + " " + idStop;

                if (result.arrives) {
                    stopArrives = new StopArrives(idStop);

                    for (var i = 0; i < result.arrives.length; i++) {
                        var arriveStop = new ArriveStop(result.arrives[i]);
                        stopArrives.addArrive(arriveStop);
                    }

                    for (var i = 0; i < stopArrives.arrives.length; i++) {
                        var arrive = stopArrives.arrives[i];

                        if (arrive.arrives.length >= 2) {
                            var html = tpl.get('arriveRow', {
                                line: arrive.lineId,
                                destination: arrive.destination,
                                firstTimeLeft: arrive.arrives[0].busTimeleft,
                                secondTimeLeft: arrive.arrives[1].busTimeleft
                            });
                            var tmp_element = document.createElement('li');
                            tmp_element.innerHTML = html;
                            searchList.appendChild(tmp_element.firstChild);
                        }
                    }

                    searchResultsContentList.scrollTop = 0;

                    // change page to searchResult
                    e.fire('changeActivePage');
                    tau.changePage('#arriveDetails-page');
                    showData();
                } else showError(texts.ERROR_CODE.DATA_ERROR);
            }, function(status) {
                console.error("searchPage::getArriveStops onError");
                //console.debug(JSON.stringify(status));
                showError(texts.ERROR_CODE.SERVER_ERROR);
            }, function(status) {
                console.error("searchPage::getArriveStops onTimeout");
                //console.debug(JSON.stringify(status));
                showError(texts.ERROR_CODE.SERVER_TIMEOUT);
            }, function(evt) {
                //console.log("searchPage::getArriveStops onProgress");
                //console.debug(JSON.stringify(evt));
            });
        }


        //**************************************************************************************//
        //**********************************SHOW/HIDE SECTIONS**********************************//
        //**************************************************************************************//

        function showData() {
            hideLoading();
            hideLocating();
            hideError();
            nearmeContent.hidden = false;
        }

        function hideData() {
            nearmeContent.hidden = true;
        }

        function showError(msg, title) {
            hideLoading();
            hideLocating();
            hideData();

            error_section.hidden = false;
            error_title.innerHTML = title || texts.ERROR_CODE.ERROR;
            error_text.innerHTML = msg || texts.ERROR_CODE.DATA_ERROR;
        }

        function hideError() {
            error_section.hidden = true;
        }

        function showLocating(msg) {
            hideError();
            hideData();

            locateme_btn.disabled = true;
            loading_section.hidden = false;
            loading_text.innerHTML = msg || texts.TEXT.LOCATING;
        }

        function hideLocating() {
            loading_section.hidden = true;
            locateme_btn.disabled = false;
        }

        function showLoading(msg) {
            hideError();
            hideData();

            loading_section.hidden = false;
            loading_text.innerHTML = msg || texts.TEXT.LOADING;
        }

        function hideLoading() {
            loading_section.hidden = true;
        }


        //**************************************************************************************//
        //***********************************INIT PAGE METHODS**********************************//
        //**************************************************************************************//

        /**
         * Show the stops page.
         */

        function _show() {
            page = page || document.getElementById(pageId);
            tau.changePage(page);
        }

        /**
         * Initialise the nearme - timer and events.
         *
         * @return {boolean} True if any action was performed.
         */

        function _init() {
            if (initialised) {
                return false;
            }

            refreshGeolocation();
            // init UI by binding events
            _bindEvents();

            initialised = true;
            return true;
        }

        function _pageShow() {
            title_time.innerHTML = dateTime.getTime();
            title_date.innerHTML = dateTime.getDate();

            refreshDatetime();
            _init();
        }

        /**
         * Bind the pageshow event.
         */

        function _bindPageShow() {
            title_time.innerHTML = dateTime.getTime();
            title_date.innerHTML = dateTime.getDate();
            document.getElementById('nearme-title').innerHTML = texts.TEXT.NEARME_TITLE.toUpperCase();
            document.getElementById('nearme-locateme').innerHTML = texts.TEXT.LOCATE_ME.toUpperCase();

            page = page || document.getElementById(pageId);
            page.addEventListener('pageshow', _pageShow);

            // if (page.classList.contains('ui-page')) {
            //     // the page is already active and the handler didn't run
            //     _pageShow();
            // }
        }

        /**
         * Refresh title datetime
         */

        function refreshDatetime() {
            dateTime.refreshDatetime(function(currentTime) {
                title_time.innerHTML = currentTime;
            });
        }

        e.listeners({
            'views.nearmePage.show': _show
        });

        return {
            init: _bindPageShow
        };
    }
});