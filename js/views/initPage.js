/*global define, tizen, window, document, history*/
/*jslint plusplus:true*/

/**
 * Init page module
 * @requires {@link core/event}
 * @requires {@link core/template}
 * @requires {@link core/application}
 * @requires {@link core/systeminfo}
 * @requires {@link helpers/dateTime}
 * @requires {@link views/stopsPage}
 * @requires {@link views/nearmePage}
 * @requires {@link views/searchPage}
 * @requires {@link views/arriveDetailsPage}
 * @requires {@link application/text}
 * @requires {@link application/config}
 * @namespace views/initPage
 * @memberof views
 */
define({
    name: 'views/initPage',
    requires: [
            'core/event',
            'core/template',
            'core/application',
            'core/systeminfo',
            'helpers/dateTime',
            'views/stopsPage',
            'views/nearmePage',
            'views/searchPage',
            'views/arriveDetailsPage',
            'application/text',
            'application/config'
    ],
    def: function viewsInitPage(req) {
        'use strict';

        var e = req.core.event,
            app = req.core.application,
            dateTime = req.helpers.dateTime,
            texts = req.application.text,
            config = req.application.config,

            stopsPage = req.views.stopsPage,
            nearmePage = req.views.nearmePage,
            searchPage = req.views.searchPage,
            searchResultsPage = req.views.searchResultsPage,
            arriveDetailsPage = req.views.arriveDetailsPage,

            sysInfo = req.core.systeminfo,

            title_time = document.getElementById('home-title-time'),
            title_date = document.getElementById('home-title-date'),

            wifi_connected = false,
            bluetooth_connected = false,
            popup_wifi = document.getElementById('popup'),
            popup_title_text = document.getElementById('popup-title'),
        	popup_content_text = document.getElementById('popup-content'),
            
            stops_btn = document.getElementById('emt-navi-stops'),
            stops_btn_icon = document.getElementById('emt-navi-stops-btn'),
            navi_stops_text = document.getElementById('emt-navi-stops-text'),

            arrives_btn = document.getElementById('emt-navi-arrives'),
            arrives_btn_icon = document.getElementById('emt-navi-arrives-btn'),
            navi_arrives_text = document.getElementById('emt-navi-arrives-text');

        //**************************************************************************************//
        //************************************EVENT HANDLERS************************************//
        //**************************************************************************************//

        /**
         * Handles hardware key tap
         * @param {event} ev
         */

        function _onHardwareKeysTap(ev) {
            var keyName = ev.keyName,
                page = document.getElementsByClassName('ui-page-active')[0],
                pageid = page ? page.id : '';

            if (keyName === 'back') {
                if (pageid === 'main-page') {
                    e.fire('visibility.change', {
                        hidden: true
                    });
                    app.exit();
                } else {
                    if (isBackToMainPage()) _pageShow();
                    history.back();
                }
            }
        }

        /**
         * Handler onVisibilityChange state
         */

        function _onVisibilityChange(ev) {
            e.fire('visibility.change', ev);
        }

        /**
         * Handler onLowBattery state
         */

        function _onLowBattery() {
            app.exit();
        }

        /**
         * Catch device PowerOff button press
         * @param {object} ev
         */

        function _onKeyDown(ev) {
            if (ev.keyIdentifier.indexOf('Power') !== -1) {
                e.fire('device.powerOff');
            }
        }

        /**
         * Handles navigation stops button tap
         * @param {event} ev
         */

        function onNaviStopsTap(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            refreshWifiStatus();

            if (!wifi_connected) {
        		if(!bluetooth_connected) {
        			popup_title_text.innerHTML = texts.TEXT.WIFI_POPUP_TITLE.toUpperCase();
                	popup_content_text.innerHTML = texts.TEXT.WIFI_POPUP_TEXT;	
            	}
            	else{
	            	popup_title_text.innerHTML = texts.TEXT.BLUETOOTH_POPUP_TITLE.toUpperCase();
	            	popup_content_text.innerHTML = texts.TEXT.BLUETOOTH_POPUP_TEXT;
            	}
        		tau.openPopup("#popup");
            }
            else {
                // change page to Stops
                e.fire('changeActivePage');
                tau.changePage('#stops-page');
            }
        }

        /**
         * Handles navigation timer search tap
         * @param {event} ev
         */

        function onNaviArrivesTap(ev) {
            ev.stopPropagation();
            ev.preventDefault();

            // change page to Search
            e.fire('changeActivePage');
            tau.changePage('#nearme-page');
        }

        function _bindEvents() {
            document.addEventListener('keydown', _onKeyDown);
            window.addEventListener('tizenhwkey', _onHardwareKeysTap);
            document.addEventListener('visibilitychange', _onVisibilityChange);
            sysInfo.listenBatteryLowState();


            stops_btn.addEventListener('click', onNaviStopsTap);
            arrives_btn.addEventListener('click', onNaviArrivesTap);
        }

        //**************************************************************************************//
        //***********************************INIT PAGE METHODS**********************************//
        //**************************************************************************************//

        /**
         * Check if back returns to main page
         */

        function isBackToMainPage() {
            return (history.length === 2);
        }

        /**
         * Refresh title datetime
         */

        function refreshDatetime() {
            title_time.innerHTML = dateTime.getTime();
            dateTime.refreshDatetime(function(currentTime) {
                title_time.innerHTML = currentTime;
            });
        }

        /**
         * Refresh wifi status
         */

        function refreshWifiStatus() {
            //check wifi settings
        	if (typeof tizen !== "undefined" && tizen.systeminfo){
        		var deviceCapabilities = tizen.systeminfo.getCapabilities();
        		if (deviceCapabilities.wifi) {
                   tizen.systeminfo.getPropertyValue('WIFI_NETWORK', function(wifi) {
                	   //wifi enabled
                	   if(wifi.status === "ON"){
                		 //if wifi SSID doesn't match to EMT wifi, warn user        
                           var ssid_regexp = config.getApiEmting('emtWifi');
                           if (ssid_regexp.test(wifi.ssid)) wifi_connected = true;
                           else wifi_connected = false;
                	   }
                	   //wifi disabled, check bluetooth status
                	   else{
                		   refreshBluetoothStatus();
                	   }
                   });
        		}
        	}        	
        }
        
        /**
         * Refresh connection via bluetooth status
         */
        function refreshBluetoothStatus() {
        	//check bluetooth settings
        	if (typeof tizen !== "undefined" && tizen.systeminfo){
        		var deviceCapabilities = tizen.systeminfo.getCapabilities();
        		if (deviceCapabilities.bluetooth) {
        			if (typeof tizen !== "undefined" && tizen.bluetooth){
        				var adapter = tizen.bluetooth.getDefaultAdapter();  
        				//bluetooth on, warn user to connect by wifi
        				if(adapter.powered) {
        					bluetooth_connected = true;
        				}
                	}
        		}	
        	}     
        }

        function _pageShow() {
            refreshDatetime();
        }

        /**
         * Initialise the initPage.
         */

        function _init() {
            title_time.innerHTML = dateTime.getTime();
            title_date.innerHTML = dateTime.getDate();

            wifi_connected = true;
            //refreshWifiStatus();
            
            document.getElementById('popup-footer').hidden = true;
            
            navi_stops_text.innerHTML = texts.TEXT.STOPS.toUpperCase();
            navi_arrives_text.innerHTML = texts.TEXT.WAIT.toUpperCase();

            // bind events to page elements
            _bindEvents();
            sysInfo.checkBatteryLowState();
        }

        e.listeners({
            'core.systeminfo.battery.low': _onLowBattery
        });

        return {
            init: _init
        };
    }

});