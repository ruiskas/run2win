/*global define, tau, document, window*/
/*jslint plusplus:true*/

/**
 * ArriveDetail page module
 * @requires {@link core/event}
 * @requires {@link core/template}
 * @requires {@link helpers/dateTime}
 * @requires {@link models/arrive}
 * @requires {@link application/text}
 * @namespace views/arriveDetailsPage
 * @memberof views
 */
define({
    name: 'views/arriveDetailsPage',
    requires: [
            'core/event',
            'core/template',
            'helpers/dateTime',
            'models/arrive',
            'application/text'
    ],
    def: function viewsArriveDetailsPage(req) {
        'use strict';

        var e = req.core.event,
            dateTime = req.helpers.dateTime,
            texts = req.application.text,

            page = null,
            pageId = 'arriveDetails-page',

            //time
            title_time = document.getElementById('arriveDetails-title-time'),
            title_date = document.getElementById('arriveDetails-title-date'),

            initialised = false;

        //**************************************************************************************//
        //***********************************INIT PAGE METHODS**********************************//
        //**************************************************************************************//

        /**
         * Show the stops page.
         */

        function _show() {
            page = page || document.getElementById(pageId);
            tau.changePage(page);
        }

        /**
         * Initialise the stops - timer and events.
         *
         * @return {boolean} True if any action was performed.
         */

        function _init() {
            if (initialised) {
                return false;
            }

            // init UI by binding events
            //_bindEvents();

            initialised = true;
            return true;
        }

        function refreshDatetime() {
            dateTime.refreshDatetime(function(currentTime) {
                title_time.innerHTML = currentTime;
            });
        }

        function _pageShow() {
            title_time.innerHTML = dateTime.getTime();
            title_date.innerHTML = dateTime.getDate();

            document.getElementById('arriveDetails-lines-title').innerHTML = texts.TEXT.LINES;
            document.getElementById('arriveDetails-minutes-title').innerHTML = texts.TEXT.MINUTES;
            document.getElementById('arriveDetails-firstbus-title').innerHTML = texts.TEXT.FIRST_BUS;
            document.getElementById('arriveDetails-secondbus-title').innerHTML = texts.TEXT.SECOND_BUS;

            refreshDatetime();
            _init();
        }

        /**
         * Bind the pageshow event.
         */

        function _bindPageShow() {
            title_time.innerHTML = dateTime.getTime();
            title_date.innerHTML = dateTime.getDate();

            page = page || document.getElementById(pageId);

            page.addEventListener('pageshow', _pageShow);

            // if (page.classList.contains('ui-page')) {
            //     // the page is already active and the handler didn't run
            //     _pageShow();
            // }
        }

        e.listeners({
            'views.arriveDetailsPage.show': _show
        });
        
        return {
            init: _bindPageShow
        };
    }
});