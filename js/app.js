/*global define*/
/*jslint plusplus:true*/

/**
 * App module
 * @requires {@link core/event}
 * @requires {@link core/application}
 * @requires {@link views/initPage}
 * @namespace app
 */

define({
    name: 'app',
    requires: [
        'core/event',
        'core/application',
        'views/initPage'
    ],
    def: function appInit(req) {
        'use strict';
        
        var e = req.core.event,
            app = req.core.application;

        function init() {
        	var appOperation = app.getCurrentApplication()
                .getRequestedAppControl()
                .appControl
                .operation;
        }

        e.listeners({
            'core.storage.idb.open': init
        });

        return {
            init: init
        };
    }
});

