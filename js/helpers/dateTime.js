/*global define, moment*/

/**
 * DateTime helper module
 * @namespace helpers/dateTime
 * @memberof helpers
 */

define({
    name: 'helpers/dateTime',
    def: function helpersDateTime() {
        'use strict';

        var _currentTime = null,
            _timer = null;

        function _init() {
            //momentjs library
            moment.locale('es', {
                months: "ENERO_FEBRERO_MARZO_ABRIL_MAYO_JUNIO_JULIO_AGOSTO_SEPTIEMBRE_OCTUBRE_NOVIEMBRE_DICIEMBRE".split("_"),
                monthsShort: "ENE_FEB_MAR_ABR_MAY_JUN_JUL_AGO_SEP_OCT_NOV_DIC".split("_"),
                weekdays: "DOMINGO_LUNES_MARTES_MIERCOLES_JUEVES_VIERNES_SABADO".split("_"),
                weekdaysShort: "DOM_LUN_MAR_MIE_JUE_VIE_SAB".split("_"),
                longDateFormat: {
                    LT: "HH:mm",
                    L: "ddd, D MMM"
                },
                week: {
                    dow: 1
                }
            });
        }

        /**
         * Get time with format "HH:mm"
         * @returns current time in "HH:mm" format
         */

        function _getTime() {
            _currentTime = moment().format('LT');
            return _currentTime;
        }

        /**
         * Get date with format "ddd, D MMM"
         * @returns current date in "ddd, D MMM" format
         */

        function _getDate() {
            return moment().format('L');
        }

        /**
         * Launch new setInterval to execute callback with _currentTime
         * @param callback function to execute on time
         * @param ms milliseconds to callback (default=1000ms)
         */

        function _refreshDatetime(callback, ms) {
            if (ms === undefined) {
                ms = 1000;
            }
            if (_timer) {
                clearInterval(_timer);
            }
            _timer = setInterval(function() {
                _currentTime = _getTime();
                if (typeof callback === "function") {
                    callback(_currentTime);
                }
            }, ms);
        }

        _init();

        return {
            getTime: _getTime,
            getDate: _getDate,
            refreshDatetime: _refreshDatetime
        };
    }
});