/*global define*/

/**
 * Page module
 * @namespace helpers/page
 * @memberof helpers
 */

define({
    name: 'helpers/page',
    def: function helpersPage() {
        'use strict';

        /**
         * Checks if the page is active
         * @param {HTMLElement} page
         * @return {boolean}
         */

        function isPageActive(page) {
            return !!(page && page.classList.contains('ui-page-active'));
        }

        return {
            isPageActive: isPageActive
        };
    }
});