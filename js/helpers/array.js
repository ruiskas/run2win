/*global define*/

/**
 * Array helper module
 * @namespace helpers/array
 * @memberof helpers
 */

define({
    name: 'helpers/array',
    def: function helpersArray() {
        'use strict';

        /**
         * Checks if the elem is an array
         * @param {Object} elem
         * @return {boolean}
         */

        function _isArray(elem) {
            return Array.isArray(elem);
        }

        /**
         * Forces elem to be an array
         * @param {Object} elem
         * @return {Array}
         */

        function _force2Array(elem) {
        	var arr = elem;
        	
            if (!_isArray(elem)) {
                var a = [];
                a.push(elem);
                arr = a;
            }
            
            return arr;
        }

        return {
            isArray: _isArray,
            force2Array: _force2Array
        };
    }
});