/*global define*/

/**
 * Timer module
 * @namespace helpers/timer
 * @memberof helpers
 */
define({
    name: 'helpers/timer',
    def: function helpersTimer() {
        'use strict';

        var _timer = null;

        /**
         * Launch new setTimeout/setInterval to execute callback
         * @param callback function to execute on timer
         * @param ms milliseconds to callback (default=1000ms)
         * @param isInterval is interval or is timeout function
         */

        function _launchTimerCallback(callback, ms, isInterval) {
            if (ms === undefined) {
                ms = 1000;
            }

            if (!isInterval) {
                //if (_timer) clearTimeout(_timer);
                _timer = setTimeout(function() {
                    if (typeof callback === "function") {
                        callback();
                    }
                }, ms);
            } else {
                if (_timer) {
                    clearInterval(_timer);
                }
                _timer = setInterval(function() {
                    if (typeof callback === "function") {
                        callback();
                    }
                }, ms);
            }
        }

        /**
         * Kills timer
         */

        function _stopTimerCallback() {
            if (_timer) {
                clearInterval(_timer);
                clearTimeout(_timer);
            }
            _timer = null;
        }

        /**
         * Returns if timer is running by a setTimeout or setInterval
         * @returns {Boolean} timer is running
         */

        function _isTimerRunning() {
            return (_timer !== null);
        }

        return {
            launchTimerCallback: _launchTimerCallback,
            stopTimerCallback: _stopTimerCallback,
            isRunning: _isTimerRunning
        };
    }
});